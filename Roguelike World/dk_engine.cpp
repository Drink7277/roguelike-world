//
//  dk_engine.cpp
//  Roguelike World
//
//  Created by Drink on 29/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "dk_engine.hpp"
#include "dk_tools.hpp"
#include "map.hpp"
#include "player.hpp"
#include "json.hpp"
#include "monster.hpp"
#include "item.hpp"

#include <string>
#include <fstream>
#include <iostream>

using namespace rouge_world;

namespace dk_engine {

    GameManager::GameManager(){}

    GameManager::~GameManager(){
        for(auto i = m_game_objects.begin(); i != m_game_objects.end(); ++i) {
            delete *i;
        }
    }

    void GameManager::init(){
        Map *map = new Map();
        Player *player = new Player(map->player_start_row, map->player_start_col);
        GameController *controller = new GameController();
        controller->init(map, player);

        m_game_objects.push_back(static_cast<GameObject *> (map));
        m_game_objects.push_back(static_cast<GameObject *> (player));
        m_game_objects.push_back(static_cast<GameObject *> (controller));
        init_monsters(map, player);
        init_items(map, player);
    }

    void GameManager::init(std::string json_file){

        size_t hp;
        std::string map_file;

        std::ifstream file (json_file);
        if (file.is_open())
        {
            auto json = nlohmann::json::parse(file);
            hp = json["hp"];
            map_file = json["map"];

            file.close();
        } else {
            throw "cannot read file";
        }

        Map *map = new Map(map_file);
        Player *player = new Player(map->player_start_row, map->player_start_col, hp);
        GameController *controller = new GameController();
        controller->init(map, player);

        m_game_objects.push_back(static_cast<GameObject *> (map));
        m_game_objects.push_back(static_cast<GameObject *> (player));
        m_game_objects.push_back(static_cast<GameObject *> (controller));
        init_monsters(map, player);
        init_items(map, player);
    }

    void GameManager::init_monsters(Map *map, Player *player){
        while (true) {
            map->find_monster_red();
            if (map->position_col != 0 && map->position_row != 0) {
                Monster *monster = new MonsterRed();
                monster->init(map->position_row, map->position_col, player);
                m_game_objects.push_back(static_cast<GameObject *> (monster));
            } else {
                break;
            }
        }

        while (true) {
            map->find_monster_blue();
            if (map->position_col != 0 && map->position_row != 0){
                Monster *monster = new MonsterBlue();
                monster->init(map->position_row, map->position_col, player);
                m_game_objects.push_back(static_cast<GameObject *> (monster));
            } else {
                break;
            }
        }
    }

    void GameManager::init_items(Map *map, Player *player){
        while (true) {
            map->find_item_health();
            if (map->position_row != 0 && map->position_col != 0) {
                Item *item = new ItemHealth();
                item->init(map->position_row, map->position_col, player);
                m_game_objects.push_back(static_cast<GameObject *> (item));
            } else {
                break;
            }
        }

        while (true) {
            map->find_item_victory();
            if (map->position_row != 0 && map->position_col != 0) {
                Item *item = new ItemVictory();
                item->init(map->position_row, map->position_col, player);
                m_game_objects.push_back(static_cast<GameObject *> (item));
            } else {
                break;
            }
        }
    }

    void GameManager::main_loop(){
        while (true) {
            draw();
            update();
        }
    }

    void GameManager::draw(){
        dk_tools::clear_console();

        for(auto i = m_game_objects.begin(); i != m_game_objects.end(); ++i) {
            (*i)->draw();
        }

        dk_tools::restore_cursor_position();
    }

    void GameManager::update(){
        for(auto i = m_game_objects.begin(); i != m_game_objects.end(); ++i) {
            (*i)->update();
        }
    }

    GameObject::GameObject() {}
    GameObject::~GameObject() {}

    void GameObject::update() {}
    void GameObject::draw() {}

    GameController::GameController() {}
    GameController::~GameController() {}

    void GameController::init(Map *map, Player *player) {
        m_map = map;
        m_player = player;
    }

    void GameController::update(){
        size_t row = 0;
        size_t col = 0;
        do {
            switch (std::cin.get()) {
                case 'w':
                    row = m_player->get_row() - 1;
                    col = m_player->get_col();
                    break;
                case 'a':
                    row = m_player->get_row();
                    col = m_player->get_col() - 1;
                    break;
                case 's':
                    row = m_player->get_row() + 1;
                    col = m_player->get_col();
                    break;
                case 'd':
                    row = m_player->get_row();
                    col = m_player->get_col() + 1;
                    break;

                default:
                    break;
            }
        } while ( !m_map->is_walkable(row, col) );
        m_player->move_to(row, col);
    }
}
