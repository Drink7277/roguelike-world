//
//  dk_engine.hpp
//  Roguelike World
//
//  Created by Drink on 29/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef dk_engine_hpp
#define dk_engine_hpp

#include <stdio.h>
#include <vector>

namespace rouge_world {
    class Map;
    class Player;
    class Monster;
}

using namespace rouge_world;

namespace dk_engine {

    class GameObject{
    public:
        GameObject();
        virtual ~GameObject();

        virtual void update();
        virtual void draw();
    
    private:
        // Copy Constructor is not supported
        GameObject(const GameObject& other);
        // Copy assignment operator is not supported
        GameObject& operator=(GameObject& other);
    };

    class GameManager{

    public:
        GameManager();
        virtual ~GameManager();

        void init();
        void init(std::string json_file);
        void init_monsters(Map *map, Player *player);
        void init_items(Map *map, Player *player);
        void main_loop();

    private:
        // Copy Constructor is not supported
        GameManager(const GameManager& other);
        // Copy assignment operator is not supported
        GameManager& operator=(GameManager& other);
        
        void draw();
        void update();

        std::vector<GameObject *> m_game_objects;
    };

    class GameController: public GameObject{
    public:
        GameController();
        ~GameController();

        void init(Map *map, Player *player);
        void update();
    private:
        // Copy Constructor is not supported
        GameController(const GameController& other);
        // Copy assignment operator is not supported
        GameController& operator=(GameController& other);
        

        Map *m_map;
        Player *m_player;
    };
}

#endif /* dk_engine_hpp */
