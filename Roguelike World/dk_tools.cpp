//
//  dk_tools.cpp
//  Roguelike World
//
//  Created by Drink on 22/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "dk_tools.hpp"

#include <iostream>


namespace dk_tools {

    void clear_console(){
        std::cout << "\033[2J\033[1;1H";
    }

    void move_cursor_to(size_t row, size_t col){
        printf("%c[%zu;%zuH", 0x1B, row, col);
    }

    void save_cursor_position(){
        printf("%c[s", 0x1B);
    }

    void restore_cursor_position(){
        printf("%c[u", 0x1B);
    }

    void set_red(){
        printf("%c[1;31m", 0x1B);
    }

    void set_blue(){
        printf("%c[1;34m", 0x1B);
    }

    void set_green(){
        printf("%c[1;32m", 0x1B);
    }

    void reset_cursor_all(){
        printf("%c[0m", 0x1B);
    }
}
