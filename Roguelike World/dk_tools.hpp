//
//  dk_tools.hpp
//  Roguelike World
//
//  Created by Drink on 22/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef dk_tools_hpp
#define dk_tools_hpp

#include <stdio.h>

namespace dk_tools {

    void clear_console();

    void move_cursor_to(size_t row, size_t col);

    void save_cursor_position();

    void restore_cursor_position();

    void set_red();

    void set_blue();

    void set_green();

    void reset_cursor_all();
}

#endif /* dk_tools_hpp */
