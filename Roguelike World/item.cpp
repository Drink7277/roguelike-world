//
//  item.cpp
//  Roguelike World
//
//  Created by Drink on 29/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "item.hpp"
#include "dk_tools.hpp"
#include "player.hpp"

using namespace dk_tools;

namespace rouge_world {
    Item::Item(): m_is_collected(false) {}
    Item::~Item() {}

    void Item::init(size_t t_row, size_t t_col, Player *t_player){
        m_row = t_row;
        m_col = t_col;
        m_player = t_player;
    }

    ItemHealth::ItemHealth(): Item() {}
    ItemHealth::~ItemHealth() {}

    void ItemHealth::draw(){
        if(m_is_collected) { return; }
        move_cursor_to(m_row+1, m_col+1);
        set_green();
        printf("%c\n", 'H');
        reset_cursor_all();
    }

    void ItemHealth::update(){
        if(m_is_collected) { return; }

        if(m_row == m_player->get_row() && m_col == m_player->get_col()){
            m_is_collected = true;
            m_player->dramage(-1);
        }
    }

    ItemVictory::ItemVictory(): Item() {}
    ItemVictory::~ItemVictory() {}

    void ItemVictory::draw(){
        if(m_is_collected) { return; }
        move_cursor_to(m_row+1, m_col+1);
        set_green();
        printf("%c\n", 'V');
        reset_cursor_all();

    }

    void ItemVictory::update(){
        if(m_is_collected) { return; }

        if(m_row == m_player->get_row() && m_col == m_player->get_col()){
            m_is_collected = true;
            printf("\n==========Victory==========\n");
            exit(0);
        }
    }
}
