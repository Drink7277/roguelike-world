//
//  item.hpp
//  Roguelike World
//
//  Created by Drink on 29/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef item_hpp
#define item_hpp

#include <stdio.h>
#include "dk_engine.hpp"

namespace rouge_world {
    class Item: public dk_engine::GameObject {

    public:
        Item();
        ~Item();

        void init(size_t t_row, size_t t_col, Player *t_player);

    protected:
        size_t m_row;
        size_t m_col;
        bool m_is_collected;

        Player *m_player;
    
    private:
        // Copy Constructor is not supported
        Item(const Item& other);
        // Copy assignment operator is not supported
        Item& operator=(Item& other);
    };

    class ItemHealth: public Item{
    public:
        ItemHealth();
        ~ItemHealth();
        void update();
        void draw();
        
    private:
        // Copy Constructor is not supported
        ItemHealth(const ItemHealth& other);
        // Copy assignment operator is not supported
        ItemHealth& operator=(ItemHealth& other);
    };

    class ItemVictory: public Item{
    public:
        ItemVictory();
        ~ItemVictory();
        void update();
        void draw();
        
    private:
        // Copy Constructor is not supported
        ItemVictory(const ItemVictory& other);
        // Copy assignment operator is not supported
        ItemVictory& operator=(ItemVictory& other);
    };
}

#endif /* item_hpp */
