//
//  main.cpp
//  Roguelike World
//
//  Created by Drink on 22/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include <iostream>
#include <curses.h>

#include "dk_engine.hpp"

using namespace std;
using namespace dk_engine;

int main(int argc, const char * argv[]) {
    const string json_file = "/Users/drink/ITGT533/Roguelike World/Roguelike World/config.json";

    GameManager game;
    game.init(json_file);
    game.main_loop();

    return 0;
}
