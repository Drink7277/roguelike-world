//
//  map.cpp
//  Roguelike World
//
//  Created by Drink on 22/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "map.hpp"
#include "player.hpp"

#include <iostream>
#include <fstream>

using namespace std;



namespace rouge_world {
    Map::Map(){
        player_start_row = 2;
        player_start_col = 2;
        m_map.push_back("+----+");
        m_map.push_back("|....|");
        m_map.push_back("|....|");
        m_map.push_back("|....|");
        m_map.push_back("|....|");
        m_map.push_back("+----+");
    }
    
    
    Map::Map(string filename){
        ifstream map_file(filename);
        
        if (!map_file.is_open()){ throw "cannot open map file"; }
        
        string line;
        while (getline(map_file, line)) {
            m_map.push_back(line);
        }
        
        map_file.close();
        init_player_position();
    }
    
    Map::~Map(){}
    
    
    
    void Map::draw(){
        for (auto it = m_map.begin(); it != m_map.end(); ++it){
            cout << *it << endl;
        }
    }
    
    void Map::init_player_position(){
        player_start_row = 1;
        player_start_col = 1;
        size_t col;
        size_t row;
        for(row = 0; row < m_map.size(); row++){
            col = m_map[row].find(DEFAULT_PLAYER_SYMBOL);
            if ( col != string::npos){
                m_map[row][col] = '.';
                player_start_row = row;
                player_start_col = col;
                return;
            }
        }
    }
    
    void Map::find_monster_red(){
        find_char('R');
    }
    
    void Map::find_monster_blue(){
        find_char('B');
    }
    
    void Map::find_item_health(){
        find_char('H');
    }
    
    void Map::find_item_victory(){
        find_char('V');
    }
    
    void Map::find_char(char symbol){
        position_row = 0;
        position_col = 0;
        size_t col;
        size_t row;
        for(row = 0; row < m_map.size(); row++){
            col = m_map[row].find(symbol);
            if ( col != string::npos){
                m_map[row][col] = '.';
                position_row = row;
                position_col = col;
                return;
            }
        }
    }
        
    
    bool Map::is_walkable(size_t row, size_t col){
        if ( row <= 0 || col <= 0 ) { return false; }
        return m_map[row][col] == '.';
    }
    
    
    
}
