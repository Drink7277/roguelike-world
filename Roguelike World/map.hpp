//
//  map.hpp
//  Roguelike World
//
//  Created by Drink on 22/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef map_hpp
#define map_hpp

#include <vector>
#include "dk_engine.hpp"

namespace rouge_world {
    class Map: public dk_engine::GameObject {
        
    public:
        Map();
        Map(std::string filename);
        
        ~Map();
        
        void draw();
        void find_monster_red();
        void find_monster_blue();
        void find_item_health();
        void find_item_victory();
        bool is_walkable(size_t row, size_t col);
        
        
        size_t player_start_row;
        size_t player_start_col;
        
        size_t position_row;
        size_t position_col;
        
    private:
        // Copy Constructor is not supported
        Map(const Map& other);
        // Copy assignment operator is not supported
        Map& operator=(Map& other);

        void find_char(char);
        
        std::vector<std::string> m_map;
        
        void init_player_position();
    };
}

#endif /* map_hpp */
