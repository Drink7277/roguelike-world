//
//  monster.cpp
//  Roguelike World
//
//  Created by Drink on 29/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "monster.hpp"
#include "dk_tools.hpp"
#include "player.hpp"

using namespace dk_tools;

namespace rouge_world {
    
    Monster::Monster() : m_is_alive(true) {}
    Monster::~Monster() {}
    
    void Monster::init(size_t t_row, size_t t_col, Player *t_player){
        m_row = t_row;
        m_col = t_col;
        m_player = t_player;
    }
    
    void Monster::update(){
        if(!m_is_alive) { return; }
        
        if(m_row == m_player->get_row() && m_col == m_player->get_col()){
            m_is_alive = false;
            m_player->dramage();
        }
    }
    
    MonsterRed::MonsterRed(): Monster(){}
    MonsterRed::~MonsterRed() {}
    
    MonsterBlue::MonsterBlue(): Monster(){}
    MonsterBlue::~MonsterBlue() {}
    
    void MonsterRed::draw(){
        if(!m_is_alive) { return; }
        move_cursor_to(m_row+1, m_col+1);
        set_red();
        printf("%c\n", 'R');
        reset_cursor_all();
    }
    
    void MonsterRed::update(){
        if(!m_is_alive) { return; }
        
        if(m_row == m_player->get_row() && m_col == m_player->get_col()){
            m_is_alive = false;
            m_player->dramage(2);
        }
    }
    
    void MonsterBlue::draw(){
        if(!m_is_alive) { return; }
        move_cursor_to(m_row+1, m_col+1);
        set_blue();
        printf("%c\n", 'B');
        reset_cursor_all();
    }
}
