//
//  monster.hpp
//  Roguelike World
//
//  Created by Drink on 29/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef monster_hpp
#define monster_hpp

#include <stdio.h>
#include "dk_engine.hpp"

namespace rouge_world {
    class Monster: public dk_engine::GameObject {
        
    public:
        Monster();
        ~Monster();
        
        void init(size_t t_row, size_t t_col, Player *t_player);
        
        void update();
        
    protected:
        size_t m_row;
        size_t m_col;
        bool m_is_alive;
        
        Player *m_player;
    
    private:
        // Copy Constructor is not supported
        Monster(const Monster& other);
        // Copy assignment operator is not supported
        Monster& operator=(Monster& other);
    };
    
    class MonsterRed: public Monster{
    public:
        MonsterRed();
        ~MonsterRed();
        void draw();
        void update();
        
    private:
        // Copy Constructor is not supported
        MonsterRed(const MonsterRed& other);
        // Copy assignment operator is not supported
        MonsterRed& operator=(MonsterRed& other);
    };
    
    class MonsterBlue: public Monster{
    public:
        MonsterBlue();
        ~MonsterBlue();
        void draw();
        
    private:
        // Copy Constructor is not supported
        MonsterBlue(const MonsterBlue& other);
        // Copy assignment operator is not supported
        MonsterBlue& operator=(MonsterBlue& other);
    };
}
#endif /* monster_hpp */
