//
//  player.cpp
//  Roguelike World
//
//  Created by Drink on 22/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "player.hpp"
#include "dk_tools.hpp"

#include <curses.h>
#include <iostream>

using namespace dk_tools;

namespace rouge_world {
    
    
    
    Player::Player() : Player(1,1, DEFAULT_PLAYER_HP) {}
    Player::Player(size_t t_row, size_t t_col) : Player(t_row, t_col, DEFAULT_PLAYER_HP) {}
    Player::Player(size_t t_row, size_t t_col, size_t t_hp) : m_row(t_row), m_col(t_col), m_hp(t_hp) {}
    
    Player::~Player() {}
    
    void Player::draw(){
        draw_hp();
        draw_position();
        draw_control();
        save_cursor_position();
        move_cursor_to(m_row+1, m_col+1);
        printf("%c\n", DEFAULT_PLAYER_SYMBOL);
    }
    
    void Player::draw_hp(){
        printf("\nHP: ");
        for(int i = 0; i < m_hp; i++){
            printf("@");
        }
        printf("\n");
    }
    
    void Player::draw_control(){
        printf("\nCONTROL:\n");
        printf("  w: UP, a: LEFT, s: DOWN, d: RIGHT\n\nCOMMAND:\n");
    }
    
    void Player::draw_position(){
        printf("(row: %zu,col: %zu)\n", m_row, m_col);
    }
    
    void Player::dramage(int n){
        m_hp -= n;
        if(m_hp != 0) { return; }
        printf("\n==========GAME OVER==========\n");
        exit(0);
    }
    
    void Player::update() {
        
    }
    
    void Player::move_left(){
        m_col--;
    }
    
    void Player::move_right(){
        m_col++;
    }
    
    void Player::move_up(){
        m_row--;
    }
    
    void Player::move_down(){
        m_row++;
    }
    
    
    size_t Player::get_row() { return m_row; }
    size_t Player::get_col() { return m_col; }
    void Player::move_to(size_t t_row, size_t t_col){
        m_row = t_row;
        m_col = t_col;
    }
    
}
