//
//  player.hpp
//  Roguelike World
//
//  Created by Drink on 22/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef player_hpp
#define player_hpp

#include <stdio.h>
#include "dk_engine.hpp"

namespace rouge_world {
    
    const char DEFAULT_PLAYER_SYMBOL = '@';
    const size_t DEFAULT_PLAYER_HP = 5;
    
    class Player: public dk_engine::GameObject  {
    public:
        Player();
        Player(size_t t_row, size_t t_col);
        Player(size_t t_row, size_t t_col, size_t t_hp);
        ~Player();
        
        void draw();
        void update();
        
        void move_left();
        void move_right();
        void move_up();
        void move_down();
        void dramage(int n = 1);
        
        size_t get_row();
        size_t get_col();
        
        void move_to(size_t t_row, size_t t_col);
    private:
        // Copy Constructor is not supported
        Player(const Player& other);
        // Copy assignment operator is not supported
        Player& operator=(Player& other);
        
        void draw_hp();
        void draw_control();
        void draw_position();
        
        size_t m_row;
        size_t m_col;
        size_t m_hp;
    };
    
}

#endif /* player_hpp */
